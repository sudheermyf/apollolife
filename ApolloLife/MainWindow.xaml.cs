﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloLife.Controls;
using ApolloDB.Models;
using ApolloLife.AdminUserControls;
using ApolloLife.Controls.AlertMessageControls;
using System.Configuration;
using System.Windows.Threading;
using System.Reflection;

namespace ApolloLife
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region variables
        private DispatcherTimer idleTimer, autoSyncTimer;
        private int idleTimerSeconds = 0;
        private Configuration config;
        public static int zIndexValue = 9999;
        public static int ivRenTrsf;
        AdminLogin AdmLogin;
        AdminSettings AdmSetting;
        ManipulationModes currentMode = ManipulationModes.All;
        #endregion
        public MainWindow()
        {
            InitializeComponent();
            ivRenTrsf = 10;
        }

        #region Methods
        private int SetZIndexValue()    //increment ZIndex value method
        {
            return zIndexValue++;
        }
        private void CheckRenderTransform()
        {
            int countIV = 0;
            foreach (Control ctrl in GrdUserControlPanel.Children)
            {
                if (ctrl is ImageView || ctrl is VideoView || ctrl is BroucherUC)
                    countIV++;
            }
            if (ivRenTrsf > 120 || countIV == 0)
                ivRenTrsf = 20;
            else
            {
                ivRenTrsf += 20;
            }
        }
        private void HomePage_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
            {
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);
            }

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }
        private void HomePage_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            if (element != null)
            {
                MatrixTransform xform = element.RenderTransform as MatrixTransform;
                Matrix matrix = xform.Matrix;
                ManipulationDelta delta = args.DeltaManipulation;
                Point center = args.ManipulationOrigin;
                matrix.Translate(-center.X, -center.Y);
                matrix.Scale(delta.Scale.X, delta.Scale.Y);
                matrix.Translate(center.X, center.Y);
                matrix.Translate(delta.Translation.X, delta.Translation.Y);
                xform.Matrix = matrix;
            }
            args.Handled = true;
            base.OnManipulationDelta(args);
        }

        #endregion
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Apps.Logger.Info("Initiated Window_Loaded() in MainWindow.xaml.cs");
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            idleTimerSeconds = (!(Int32.TryParse(config.AppSettings.Settings["IdleTimer"].Value, out idleTimerSeconds)) && idleTimerSeconds <= 120) ? 120 : idleTimerSeconds;
            idleTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);
            idleTimer.Interval = new TimeSpan(0, 0, 5);
            idleTimer.Tick += idleTimer_Tick;
            InitiateAutoSync();

            LoadSlideShowControl();
            #region Manipulation Events
            this.ManipulationStarting -= HomePage_ManipulationStarting;
            this.ManipulationDelta -= HomePage_ManipulationDelta;
            this.ManipulationStarting += HomePage_ManipulationStarting;
            this.ManipulationDelta += HomePage_ManipulationDelta;
            #endregion

            Apps.Logger.Info("Ends Window_Loaded() in MainWindow.xaml.cs");
        }
        void idleTimer_Tick(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated idleTimer_Tick() in MainWindow.xaml.cs");
            TimeSpan span = IdleTimeSetter.GetLastInput();
            if (idleTimerSeconds <= (Convert.ToInt32(span.TotalSeconds)))
            {
                GrdUserControlPanel.Children.Clear();
                LoadSlideShowControl();
            }
            Apps.Logger.Info("Ends idleTimer_Tick() in MainWindow.xaml.cs");
        }

        private void LoadHomeLogo()
        {
            Apps.Logger.Info("Initiated btnLogo_PreviewTouchUp() in MainWindow.xaml.cs");
            LoadSlideShowControl();
            Apps.Logger.Info("Ends btnLogo_PreviewTouchUp() in MainWindow.xaml.cs");
        }

        private void LoadSlideShowControl()
        {
            Apps.Logger.Info("Initiated LoadSlideShowControl() in MainWindow.xaml.cs");

            zIndexValue = 9999;
            GrdSlideShowPanel.Children.Clear();
            GrdUserControlPanel.Children.Clear();
            GrdMenuControlPanel.Children.Clear();
            GrdAlertControlPanel.Children.Clear();
            GrdAdminUserControlPanel.Children.Clear();            

            SlideShowUC slideShow = new SlideShowUC();
            GrdSlideShowPanel.Children.Add(slideShow);
            slideShow.EvntCloseSlideShow += slideShow_EvntCloseSlideShow;
            slideShow.EvntOpenAdminSettings += slideShow_EvntOpenAdminSettings;
            idleTimer.Stop();

            Apps.Logger.Info("Ends LoadSlideShowControl() in MainWindow.xaml.cs");
        }

        void slideShow_EvntOpenAdminSettings(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated slide_EvntOpenAdminSettings() in MainWindow.xaml.cs");
            LoadAdminLogin();
            Apps.Logger.Info("Ends slide_EvntOpenAdminSettings() in MainWindow.xaml.cs");
        }

        void slideShow_EvntCloseSlideShow(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated slideShow_EvntCloseSlideShow() in MainWindow.xaml.cs");
            SlideShowUC slide = (SlideShowUC)sender;
            GrdSlideShowPanel.Children.Remove(slide);            
            GrdSlideShowPanel.Children.Clear();
            LoadMenuControl();
            idleTimer.Start();
            Apps.Logger.Info("Ends slideShow_EvntCloseSlideShow() in MainWindow.xaml.cs");
        }
        

        private void LoadMenuControl()
        {
            MenuUC menu = new MenuUC();
            GrdSlideShowPanel.Children.Clear();
            GrdUserControlPanel.Children.Clear();
            GrdMenuControlPanel.Children.Add(menu);
            menu.EvntCloseMenuUC += menu_EvntCloseMenuUC;
            menu.EvntOpenCategoryItem += menu_EvntOpenCategoryItem;
            menu.EvntContactUs += menu_EvntContactUs;
        }

        void menu_EvntContactUs(object sender, EventArgs e)
        {
            LoadContactUs();
        }

        void menu_EvntCloseMenuUC(object sender, EventArgs e)
        {
            GrdMenuControlPanel.Children.Clear();
            zIndexValue = 9999;
            LoadSlideShowControl();
        }

        void menu_EvntOpenCategoryItem(object sender, EventArgs e)
        {
            Category category = sender as Category;
            if (category != null && category.IsActive.Equals(true))
            {
                if (category.LstSubCategories != null && category.LstSubCategories.Count > 0)
                {
                    //Load category page
                    if (ValidateCategoryExists(category))
                    {
                        CategoryUC categUC = new CategoryUC();
                        categUC.categoryItem = category;
                        categUC.BringIntoView();
                        Panel.SetZIndex(categUC, SetZIndexValue());
                        GrdUserControlPanel.Children.Add(categUC);
                        categUC.EvntCloseCategoryUC += categUC_EvntCloseCategoryUC;
                        categUC.EvntOpenSubCategItem += categUC_EvntOpenSubCategItem;
                        categUC.EvntHomeLogo += categUC_EvntHomeLogo;
                        categUC.EvntContactUs += categUC_EvntContactUs;
                    }
                }
                else if (category.LstDocuments != null && category.LstDocuments.Count > 0)
                {
                    //load document page
                    List<Document> lstDocument = category.LstDocuments.Where(b => b.IsActive).ToList();
                    LoadDocument(lstDocument, category.Name);
                }
                else
                {
                    LoadUnderDevelopmentUC();
                }
            }
        }

        void categUC_EvntContactUs(object sender, EventArgs e)
        {
            LoadContactUs();
        }

        void categUC_EvntHomeLogo(object sender, EventArgs e)
        {
            LoadHomeLogo();
        }

        void categUC_EvntOpenSubCategItem(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated categUC_EvntOpenSubCategItem() in MainWindow.xaml.cs");
            Button btnSubCategItem = sender as Button;
            if (btnSubCategItem != null)
            {
                SubCategory subCategItem = btnSubCategItem.DataContext as SubCategory;
                if (subCategItem != null)
                {
                    if (subCategItem.LstDocuments != null && subCategItem.LstDocuments.Count > 0)
                    {
                        List<Document> lstDocument = subCategItem.LstDocuments.Where(c => c.IsActive).ToList();
                        LoadDocument(lstDocument, subCategItem.Name);
                    }
                    else
                    {
                        LoadUnderDevelopmentUC();
                    }
                }
            }
            Apps.Logger.Info("Ends categUC_EvntOpenSubCategItem() in MainWindow.xaml.cs");
        }

        void categUC_EvntCloseCategoryUC(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated categUC_EvntCloseCategoryUC() in MainWindow.xaml.cs");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GrdUserControlPanel.Children)
            {
                if (item is CategoryUC || item is ImageView ||
                    item is VideoView || item is BroucherUC)
                {
                    lstIndexes.Add(GrdUserControlPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GrdUserControlPanel.Children.RemoveAt(c);   //remove usercontrols by its index values
            });
            Apps.Logger.Info("Ends categUC_EvntCloseCategoryUC() in MainWindow.xaml.cs");
        }

        private void LoadDocument(List<Document> lstDocument, string subCategName)
        {
            Apps.Logger.Info("Initiated LoadDocument() in MainWindow.xaml.cs");
            string docType = string.Empty;
            if (lstDocument != null && lstDocument.Count == 1)
            {
                lstDocument.Where(b => b.IsActive).ToList().ForEach(c =>
                {
                    docType = LoadByDocumentType(c);
                });

            }
            else if (lstDocument.Where(b => b.IsActive).Any(c => c.DocumentType.Equals("PPT") || c.DocumentType.Equals("PDF")))
            {
                LoadDocumentUC(lstDocument, subCategName);
            }
            else if (lstDocument.Count > 1 && lstDocument.Where(b => b.IsActive).Any(c => c.DocumentType.Equals("Video")))
            {
                LoadDocumentUC(lstDocument, subCategName);
            }
            else
            {
                lstDocument.Where(b => b.IsActive).ToList().ForEach(c =>
                {
                    docType = LoadByDocumentType(c);
                });
            }
            Apps.Logger.Info("Ends LoadDocument() in MainWindow.xaml.cs");
        }

        private string LoadByDocumentType(Document document)
        {
            Apps.Logger.Info("Initiated LoadByDocumentType() in MainWindow.xaml.cs");
            string docType;
            docType = document.DocumentType;
            switch (docType)
            {
                case "Image":
                    LoadImageControl(document);
                    break;
                case "Video":
                    LoadVideoControl(document);
                    break;
                case "PPT":
                case "PDF":
                    LoadPresentationControl(document);
                    break;
                default:
                    break;
            }
            Apps.Logger.Info("Ends LoadByDocumentType() in MainWindow.xaml.cs");
            return docType;
        }

        private void LoadDocumentUC(List<Document> lstDocument, string subCategName)
        {
            Apps.Logger.Info("Initiated LoadDocumentUC() in MainWindow.xaml.cs");
            bool isExists = false;
            foreach (UIElement item in GrdUserControlPanel.Children)   //check weather DocumentsUC already exists
            {
                if (item is DocumentsUC)
                {
                    Apps.Logger.Info("Ends LoadDocumentUC() -> Already Exists in MainWindow.xaml.cs");
                    isExists = true;
                }
            }
            if (!isExists)
            {
                lstDocument = lstDocument.Where(c => c.LstDocDetails != null && c.LstDocDetails.Count > 0 && c.IsActive).ToList();
                if (lstDocument != null && lstDocument.Count > 0)
                {
                    DocumentsUC docUC = new DocumentsUC();
                    docUC.ICDocuments.ItemsSource = lstDocument;
                    docUC.tbCategoryName.Text = subCategName;
                    docUC.BringIntoView();
                    Panel.SetZIndex(docUC, SetZIndexValue());
                    GrdUserControlPanel.Children.Add(docUC);
                    docUC.EvntOpenDocumentItem += docUC_EvntOpenDocumentItem;
                    docUC.EvntCloseDocumentUC += docUC_EvntCloseDocumentUC;
                    docUC.EvntHomeLogo += docUC_EvntHomeLogo;
                    docUC.EvntContactUs += docUC_EvntContactUs;
                }
                else
                {
                    LoadUnderDevelopmentUC();
                }
            }
            Apps.Logger.Info("Ends LoadDocumentUC() in MainWindow.xaml.cs");
        }

        void docUC_EvntContactUs(object sender, EventArgs e)
        {
            LoadContactUs();
        }

        void docUC_EvntHomeLogo(object sender, EventArgs e)
        {
            LoadHomeLogo();
        }

        void docUC_EvntCloseDocumentUC(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated docUC_EvntCloseDocumentUC() in MainWindow.xaml.cs");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GrdUserControlPanel.Children)
            {
                if (item is DocumentsUC || item is ImageView ||
                    item is VideoView || item is BroucherUC)
                {
                    lstIndexes.Add(GrdUserControlPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GrdUserControlPanel.Children.RemoveAt(c);   //remove usercontrols by its index values
            });
            Apps.Logger.Info("Ends docUC_EvntCloseDocumentUC() in MainWindow.xaml.cs");
        }

        void docUC_EvntOpenDocumentItem(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated docUC_EvntOpenDocumentItem() in MainWindow.xaml.cs");
            Button btn = sender as Button;
            if (btn != null && btn.DataContext != null)
            {
                Document selectedDocument = btn.DataContext as Document;
                LoadByDocumentType(selectedDocument);
            }
            Apps.Logger.Info("Ends docUC_EvntOpenDocumentItem() in MainWindow.xaml.cs");
        }


        private bool ValidateCategoryExists(Category category)
        {
            Apps.Logger.Info("Initiated ValidateCategoryExists() in MainWindow.xaml.cs");
            foreach (Control item in GrdUserControlPanel.Children)
            {
                if (item is CategoryUC)
                {
                    item.BringIntoView();
                    Panel.SetZIndex(item, SetZIndexValue());
                    Apps.Logger.Info("Ends ValidateCategoryExists() -> Already Exists in MainWindow.xaml.cs");
                    return false;
                }
            }
            Apps.Logger.Info("Ends ValidateCategoryExists() in MainWindow.xaml.cs");
            return true;
        }
       
        void category_EvntCloseCategoryUC(object sender, EventArgs e)
        {
            CategoryUC category = (CategoryUC)sender;
            GrdUserControlPanel.Children.Remove(category);
        }

        #region AdminUserControlMethods
        
        private void LoadAdminLogin()
        {
            Apps.Logger.Info("Initiated LoadAdminLogin() in MainWindow.xaml.cs");
            bool isExists = false;
            foreach (UIElement item in GrdAdminUserControlPanel.Children)   //check weather AdminLogin exists
            {
                if (item is AdminLogin)
                {
                    Apps.Logger.Info("Ends LoadAdminLogin() -> Already Exists in MainWindow.xaml.cs");
                    isExists = true;
                }
            }
            if (!isExists)
            {
                //load AdminLogin
                AdmLogin = new AdminLogin();
                GrdAdminUserControlPanel.Children.Clear();
                GrdAdminUserControlPanel.Children.Add(AdmLogin);
                //events in AdminLogin UC
                AdmLogin.EvntCloseAdminSetting += AdmLogin_EvntCloseAdminSetting;
                AdmLogin.EvntLoginAdmin += AdmLogin_EvntLoginAdmin;
                AdmLogin.EvntWarningAlert += AdmLogin_EvntWarningAlert;
            }
            Apps.Logger.Info("Ends LoadAdminLogin() in MainWindow.xaml.cs");
        }

        void AdmLogin_EvntWarningAlert(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated AdmLogin_EvntWarningAlert() in MainWindow.xaml.cs");
            LoadWarningAlert(sender);
            Apps.Logger.Info("Ends AdmLogin_EvntWarningAlert() in MainWindow.xaml.cs");
        }
        void AdmLogin_EvntCloseAdminSetting(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated AdmLogin_EvntCloseAdminSetting() in MainWindow.xaml.cs");
            //clear AdminSettings panel
            GrdAdminUserControlPanel.Children.Clear();
            Apps.Logger.Info("Ends AdmLogin_EvntCloseAdminSetting() in MainWindow.xaml.cs");
        }
        void AdmLogin_EvntLoginAdmin(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated AdmLogin_EvntLoginAdmin() in MainWindow.xaml.cs");
            LoadAdminSettings();    //load AdminSettings
            Apps.Logger.Info("Ends AdmLogin_EvntLoginAdmin() in MainWindow.xaml.cs");
        }
        private void LoadAdminSettings()
        {
            Apps.Logger.Info("Initiated LoadAdminSettings() in MainWindow.xaml.cs");
            bool isExists = false;
            foreach (UIElement item in GrdAdminUserControlPanel.Children)   //check weather AdminSettings already exists
            {
                if (item is AdminSettings)
                {
                    Apps.Logger.Info("Ends LoadAdminSettings() -> Already Exists in MainWindow.xaml.cs");
                    isExists = true;
                }
            }
            if (!isExists)
            {
                //load AdminSettings
                AdmSetting = new AdminSettings();
                GrdAdminUserControlPanel.Children.Clear();
                GrdAdminUserControlPanel.Children.Add(AdmSetting);
                //load events in AdminSettings
                AdmSetting.EvntLogoutSettings += AdmSetting_EvntLogoutSettings;
                AdmSetting.EvntSlideShowIdleTimer += AdmSetting_EvntSlideShowIdleTimer;
                AdmSetting.EvntOpenSetAutoSync += AdmSetting_EvntOpenSetAutoSync;
            }
            Apps.Logger.Info("Ends LoadAdminSettings() in MainWindow.xaml.cs");
        }

        void AdmSetting_EvntOpenSetAutoSync(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated AdmSetting_EvntOpenSetAutoSync() in MainWindow.xaml.cs");
            //load SetAutoSyncTime
            GrdAdminUserControlPanel.Children.Clear();
            SetAutoSyncTime autosync = new SetAutoSyncTime();
            GrdAdminUserControlPanel.Children.Add(autosync);
            //events to raise in SetAutoSyncTime
            autosync.EvntCloseSetAutoSync += autosync_EvntCloseSetAutoSync;
            autosync.autoSyncRefresh += autosync_autoSyncRefresh;
            autosync.EvntSuccessAlert += autosync_EvntSuccessAlert;
            autosync.EvntWarningAlert += autosync_EvntWarningAlert;
            Apps.Logger.Info("Ends AdmSetting_EvntOpenSetAutoSync() in MainWindow.xaml.cs");
        }

        void autosync_EvntWarningAlert(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated autosync_EvntWarningAlert() in MainWindow.xaml.cs");
            LoadWarningAlert(sender);
            Apps.Logger.Info("Ends autosync_EvntWarningAlert() in MainWindow.xaml.cs");
        }

        void autosync_EvntSuccessAlert(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated autosync_EvntSuccessAlert() in MainWindow.xaml.cs");
            LoadSuccessAlert(sender);
            Apps.Logger.Info("Ends autosync_EvntSuccessAlert() in MainWindow.xaml.cs");
        }

        void autosync_autoSyncRefresh(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated autosync_autoSyncRefresh() in MainWindow.xaml.cs");
            InitiateAutoSync();
            Apps.Logger.Info("Ends autosync_autoSyncRefresh() in MainWindow.xaml.cs");
        }
        void autosync_EvntCloseSetAutoSync(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated autosync_EvntCloseSetAutoSync() in MainWindow.xaml.cs");
            //remove SetAutoSyncTime in GrdAdminUserControlPanel
            SetAutoSyncTime autosync = (SetAutoSyncTime)sender;
            GrdAdminUserControlPanel.Children.Remove(autosync);
            LoadAdminSettings();
            Apps.Logger.Info("Ends autosync_EvntCloseSetAutoSync() in MainWindow.xaml.cs");
        }
        void AdmSetting_EvntLogoutSettings(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated AdmSetting_EvntLogoutSettings() in MainWindow.xaml.cs");
            //clear GrdAdminUserControlPanel
            GrdAdminUserControlPanel.Children.Clear();
            Apps.Logger.Info("Ends AdmSetting_EvntLogoutSettings() in MainWindow.xaml.cs");
        }
        void AdmSetting_EvntSlideShowIdleTimer(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated AdmSetting_EvntSlideShowIdleTimer() in MainWindow.xaml.cs");
            GrdAdminUserControlPanel.Children.Clear();
            //load SlideShowIdleTimer
            SlideShowIdleTimer ssIdleTimer = new SlideShowIdleTimer();
            GrdAdminUserControlPanel.Children.Add(ssIdleTimer);
            //Raise events from SlideShowIdleTimer control
            ssIdleTimer.EvntCloseIdleTimer += ssIdleTimer_EvntCloseIdleTimer;
            ssIdleTimer.EvntSuccessAlert += ssIdleTimer_EvntSuccessAlert;
            ssIdleTimer.EvntWarningAlert += ssIdleTimer_EvntWarningAlert;
            Apps.Logger.Info("Ends AdmSetting_EvntSlideShowIdleTimer() in MainWindow.xaml.cs");
        }

        void ssIdleTimer_EvntWarningAlert(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated ssIdleTimer_EvntWarningAlert() in MainWindow.xaml.cs");
            LoadWarningAlert(sender);
            Apps.Logger.Info("Ends ssIdleTimer_EvntWarningAlert() in MainWindow.xaml.cs");
        }

        void ssIdleTimer_EvntSuccessAlert(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated ssIdleTimer_EvntSuccessAlert() in MainWindow.xaml.cs");
            LoadSuccessAlert(sender);
            Apps.Logger.Info("Ends ssIdleTimer_EvntSuccessAlert() in MainWindow.xaml.cs");
        }

        void ssIdleTimer_EvntCloseIdleTimer(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated ssIdleTimer_EvntCloseIdleTimer() in MainWindow.xaml.cs");
            SlideShowIdleTimer ssIdleTimer = (SlideShowIdleTimer)sender;
            GrdAdminUserControlPanel.Children.Remove(ssIdleTimer);
            LoadAdminSettings();
            Apps.Logger.Info("Ends ssIdleTimer_EvntCloseIdleTimer() in MainWindow.xaml.cs");
        }

        #endregion

        #region AutoSyncMethod
        private void InitiateAutoSync()
        {
            Apps.Logger.Info("Initiated InitiateAutoSync() in MainWindow.xaml.cs");
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                if (Convert.ToBoolean(config.AppSettings.Settings["AutoSyncEnabled"].Value))
                {
                    DateTime autoSyncTime = DateTime.Now;

                    if (DateTime.TryParse(config.AppSettings.Settings["AutoSyncTiming"].Value, out autoSyncTime))
                    {
                        string syncTime = autoSyncTime.ToString("HH:mm");
                        if (autoSyncTimer != null)
                        {
                            autoSyncTimer.Stop();
                            autoSyncTimer = null;
                        }
                        DateTime fromDate = DateTime.Now;
                        DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + syncTime);

                        TimeSpan tspan = fetchSyncDate - fromDate;

                        if (tspan.Seconds < 0)
                        {
                            fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + syncTime);
                            tspan = fetchSyncDate - fromDate;
                        }

                        if (autoSyncTimer == null)
                        {
                            autoSyncTimer = new DispatcherTimer();
                            autoSyncTimer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                            autoSyncTimer.Tick += autoSyncTimer_Tick;
                            autoSyncTimer.Start();
                        }
                    }
                }
                Apps.Logger.Info("Ends InitiateAutoSync() in MainWindow.xaml.cs");
            }
            catch (Exception ex)
            {
                Apps.Logger.Info("InitiateAutoSync() -> Catch Exception : " + ex.Message + " in MainWindow.xaml.cs");
                //Log Here
            }
        }
        void autoSyncTimer_Tick(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated autoSyncTimer_Tick() in MainWindow.xaml.cs");
            if (!CheckAutosyncExists())
            {
                AutoSyncUC sync = new AutoSyncUC();
                GrdSyncControlPanel.Children.Add(sync);
                sync.EvntCloseSetAutoSync += sync_EvntCloseSetAutoSync;
            }
            autoSyncTimer.Stop();
            Apps.Logger.Info("Ends autoSyncTimer_Tick() in MainWindow.xaml.cs");
        }
        private bool CheckAutosyncExists()  //method to check autosyncuc already exists or not
        {
            Apps.Logger.Info("Initiated CheckAutosyncExists() in MainWindow.xaml.cs");
            foreach (UIElement item in GrdSyncControlPanel.Children)
            {
                if (item is AutoSyncUC)
                {
                    Apps.Logger.Info("Ends CheckAutosyncExists() -> Already Exists in MainWindow.xaml.cs");
                    return true;
                }
            }
            Apps.Logger.Info("Ends CheckAutosyncExists() in MainWindow.xaml.cs");
            return false;
        }
        void sync_EvntCloseSetAutoSync(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated sync_EvntCloseSetAutoSync() in MainWindow.xaml.cs");
            //remove autosyncuc from grdsynccontrolpanel after sync
            AutoSyncUC sync = (AutoSyncUC)sender;
            GrdSyncControlPanel.Children.Remove(sync);
            Apps.Logger.Info("Ends sync_EvntCloseSetAutoSync() in MainWindow.xaml.cs");
        }
        #endregion

        #region AlertMessageMethods
        private void LoadSuccessAlert(object sender)
        {
            Apps.Logger.Info("Initiated LoadSuccessAlert() in MainWindow.xaml.cs");
            SuccessAlertUC sAlert = new SuccessAlertUC();
            string message = sender as string;
            sAlert.tblSuccessMessage.Text = message;
            GrdAlertControlPanel.Children.Add(sAlert);
            //raise events here
            sAlert.EvntCloseSuccessAlert += sAlert_EvntCloseSuccessAlert;
            Apps.Logger.Info("Ends LoadSuccessAlert() in MainWindow.xaml.cs");
        }

        void sAlert_EvntCloseSuccessAlert(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated sAlert_EvntCloseSuccessAlert() in MainWindow.xaml.cs");
            SuccessAlertUC sAlert = (SuccessAlertUC)sender;
            GrdAlertControlPanel.Children.Remove(sAlert);
            Apps.Logger.Info("Ends sAlert_EvntCloseSuccessAlert() in MainWindow.xaml.cs");
        }

        private void LoadErrorAlert(object sender)
        {
            Apps.Logger.Info("Initiated LoadErrorAlert() in MainWindow.xaml.cs");
            ErrorAlertUC eAlert = new ErrorAlertUC();
            string message = sender as string;
            eAlert.tblErrorMessage.Text = message;
            GrdAlertControlPanel.Children.Add(eAlert);
            //raise events here
            eAlert.EvntCloseErrorAlert += eAlert_EvntCloseErrorAlert;
            Apps.Logger.Info("Ends LoadErrorAlert() in MainWindow.xaml.cs");
        }

        void eAlert_EvntCloseErrorAlert(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated eAlert_EvntCloseErrorAlert() in MainWindow.xaml.cs");
            ErrorAlertUC eAlert = (ErrorAlertUC)sender;
            GrdAlertControlPanel.Children.Remove(eAlert);
            Apps.Logger.Info("Ends eAlert_EvntCloseErrorAlert() in MainWindow.xaml.cs");
        }

        private void LoadWarningAlert(object sender)
        {
            Apps.Logger.Info("Initiated LoadWarningAlert() in MainWindow.xaml.cs");
            WarningAlertUC wAlert = new WarningAlertUC();
            string message = sender as string;
            wAlert.tblWarningText.Text = message;
            GrdAlertControlPanel.Children.Add(wAlert);
            //raise events here
            wAlert.EvntCloseWarningAlert += wAlert_EvntCloseWarningAlert;
            Apps.Logger.Info("Ends LoadWarningAlert() in MainWindow.xaml.cs");
        }

        void wAlert_EvntCloseWarningAlert(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated wAlert_EvntCloseWarningAlert() in MainWindow.xaml.cs");
            WarningAlertUC wAlert = (WarningAlertUC)sender;
            GrdAlertControlPanel.Children.Remove(wAlert);
            Apps.Logger.Info("Ends wAlert_EvntCloseWarningAlert() in MainWindow.xaml.cs");
        }

        private void LoadUnderDevelopmentUC()
        {
            UnderDevelopmentUC dev = new UnderDevelopmentUC();
            GrdAlertControlPanel.Children.Add(dev);
            dev.EvntCloseDevelopmentUC += dev_EvntCloseDevelopmentUC;
        }

        void dev_EvntCloseDevelopmentUC(object sender, EventArgs e)
        {
            UnderDevelopmentUC dev = (UnderDevelopmentUC)sender;
            GrdAlertControlPanel.Children.Remove(dev);
        }
        #endregion

        #region LoadBroucherControlMethod
        private void LoadPresentationControl(Document selectedDocument)
        {
            Apps.Logger.Info("Initiated LoadPresentationControl() in MainWindow.xaml.cs");
            if (ValidateBroucher(selectedDocument))
            {
                //Load PresentationControl usercontrol
                if (selectedDocument != null && selectedDocument.LstDocDetails != null && selectedDocument.LstDocDetails.Count > 0)
                {
                    CheckRenderTransform();
                    BroucherUC broucher = new BroucherUC();
                    broucher.IsManipulationEnabled = true;
                    broucher.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                    broucher.BringIntoView();
                    Panel.SetZIndex(broucher, SetZIndexValue());
                    broucher.lstCategoryData = selectedDocument.LstDocDetails.Where(c => c.IsActive).ToList();
                    GrdUserControlPanel.Children.Add(broucher);
                    broucher.EvntCloseBroucher -= broucher_EvntCloseBroucher;
                    broucher.EvntCloseBroucher += broucher_EvntCloseBroucher;
                }
            }
            Apps.Logger.Info("Ends LoadPresentationControl() in MainWindow.xaml.cs");
        }

        private bool ValidateBroucher(Document docItem)
        {
            Apps.Logger.Info("Initiated ValidateBroucher() in MainWindow.xaml.cs");
            int count = 0;
            foreach (Control item in GrdUserControlPanel.Children)
            {
                if (item is BroucherUC)
                {
                    docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                    {
                        BroucherUC broucher = item as BroucherUC;
                        broucher.lstCategoryData.ForEach(d =>
                        {
                            if (d.ID.Equals(c.ID))
                            {
                                item.BringIntoView();
                                Panel.SetZIndex(item, SetZIndexValue());
                                count++;
                                Apps.Logger.Info("Ends ValidateBroucher() -> Already Exists in MainWindow.xaml.cs.cs");
                            }
                        });
                    });
                }
            }
            if (count > 0)
            {
                Apps.Logger.Info("Ends ValidateBroucher() -> Already Exists in MainWindow.xaml.cs");
                return false;
            }
            else
            {
                Apps.Logger.Info("Ends ValidateBroucher() in MainWindow.xaml.cs");
                return true;
            }
        }
        void broucher_EvntCloseBroucher(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated broucher_EvntCloseBroucher() in MainWindow.xaml.cs");
            BroucherUC broucher = (BroucherUC)sender;
            GrdUserControlPanel.Children.Remove(broucher);
            Apps.Logger.Info("Ends broucher_EvntCloseBroucher() in MainWindow.xaml.cs");
        }
        #endregion

        #region LoadImageControlMethod
        private void LoadImageControl(Document docItem)
        {
            Apps.Logger.Info("Initiated LoadImageControl() in MainWindow.xaml.cs");
            if (ValidateImageView(docItem))
            {
                docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                {
                    CheckRenderTransform();
                    ImageView imgView = new ImageView();
                    imgView.IsManipulationEnabled = true;
                    imgView.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                    imgView.BringIntoView();
                    imgView.documentDets = c;
                    Panel.SetZIndex(imgView, SetZIndexValue());
                    imgView.DataContext = c;
                    //imgView.img.Source = new BitmapImage(new Uri(c.FileLocation));
                    GrdUserControlPanel.Children.Add(imgView);
                    //raise events from ImageView
                    imgView.EvntCloseImageView += imgView_EvntCloseImageView;
                });
            }
            Apps.Logger.Info("Ends LoadImageControl() in MainWindow.xaml.cs");
        }

        private bool ValidateImageView(Document docItem)
        {
            Apps.Logger.Info("Initiated ValidateImageView() in MainWindow.xaml.cs");
            int count = 0;
            foreach (Control item in GrdUserControlPanel.Children)
            {
                if (item is ImageView)
                {
                    docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                    {
                        DocumentDetails doc = item.DataContext as DocumentDetails;
                        if (doc.ID.Equals(c.ID))
                        {
                            item.BringIntoView();
                            Panel.SetZIndex(item, SetZIndexValue());
                            count++;
                        }
                    });
                }
            }
            if (count > 0)
            {
                Apps.Logger.Info("Ends ValidateImageView() -> Already Exists in MainWindow.xaml.cs"); return false;
            }
            else
            {
                Apps.Logger.Info("Ends ValidateImageView() in MainWindow.xaml.cs");
                return true;
            }
        }

        void imgView_EvntCloseImageView(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated imgView_EvntCloseImageView() in MainWindow.xaml.cs");
            //remove ImageView fro GrdUserControlPanel
            ImageView imgView = (ImageView)sender;
            GrdUserControlPanel.Children.Remove(imgView);
            Apps.Logger.Info("Ends imgView_EvntCloseImageView() in MainWindow.xaml.cs");
        }
        #endregion

        #region LoadVideoControlMethod
        private void LoadVideoControl(Document docItem)
        {
            Apps.Logger.Info("Initiated LoadVideoControl() in MainWindow.xaml.cs");
            //Load video usercontrol here
            if (ValidateVideoView(docItem))
            {
                docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                {
                    CheckRenderTransform();
                    VideoView vdoView = new VideoView();
                    vdoView.IsManipulationEnabled = true;
                    vdoView.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                    vdoView.BringIntoView();
                    Panel.SetZIndex(vdoView, SetZIndexValue());
                    vdoView.documentDets = c;
                    GrdUserControlPanel.Children.Add(vdoView);
                    //raise events from VideoView
                    vdoView.EvntCloseVideoView += vdoView_EvntCloseVideoView;
                });
            }
            Apps.Logger.Info("Ends LoadVideoControl() in MainWindow.xaml.cs");
        }

        private bool ValidateVideoView(Document docItem)
        {
            Apps.Logger.Info("Initiated ValidateVideoView() in MainWindow.xaml.cs");
            int count = 0;
            foreach (Control item in GrdUserControlPanel.Children)
            {
                if (item is VideoView)
                {
                    docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                    {
                        DocumentDetails doc = item.DataContext as DocumentDetails;
                        if (doc != null)
                        {
                            if (doc.ID.Equals(c.ID))
                            {
                                item.BringIntoView();
                                Panel.SetZIndex(item, SetZIndexValue());
                                count++;
                            }
                        }
                    });
                }
            }
            if (count > 0)
            {
                Apps.Logger.Info("Ends ValidateVideoView() -> Already Exists in MainWindow.xaml.cs"); return false;
            }
            else
            {
                Apps.Logger.Info("Ends ValidateVideoView() in MainWindow.xaml.cs");
                return true;
            }
        }

        void vdoView_EvntCloseVideoView(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated vdoView_EvntCloseVideoView() in MainWindow.xaml.cs");
            //remove VideoView from GrdUserControlPanel
            VideoView vdoView = (VideoView)sender;
            GrdUserControlPanel.Children.Remove(vdoView);
            Apps.Logger.Info("Ends vdoView_EvntCloseVideoView() in MainWindow.xaml.cs");
        }
        #endregion

        #region LoadContactUsMethod
        private void LoadContactUs()
        {
            Apps.Logger.Info("Initiated btnContactUs_PreviewTouchUp() in MainWindow.xaml.cs");
            bool isExists = false;
            foreach (UIElement item in GrdUserControlPanel.Children)    //check weather ContactUC page exists
            {
                if (item is ContactUC)
                {
                    Panel.SetZIndex(item, SetZIndexValue());
                    Apps.Logger.Info("Ends btnContactUs_PreviewTouchUp() -> Already Exists in MainWindow.xaml.cs");
                    isExists = true;
                }
            }
            if (!isExists)
            {
                //load ContactUC page
                ContactUC contact = new ContactUC();
                contact.RenderTransform = new MatrixTransform(1, 0, 0, 1, 1, 1);
                contact.BringIntoView();
                Panel.SetZIndex(contact, SetZIndexValue());
                GrdUserControlPanel.Children.Add(contact);
                //raise events of ContactUC
                contact.EvntCloseContactUs += contact_EvntCloseContactUs;
            }
            Apps.Logger.Info("Ends btnContactUs_PreviewTouchUp() in MainWindow.xaml.cs");
        }
        void contact_EvntCloseContactUs(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated contact_EvntCloseContactUs() in MainWindow.xaml.cs");
            //remove ContactUC from GrdUserControlPanel
            ContactUC contact = (ContactUC)sender;
            GrdUserControlPanel.Children.Remove(contact);
            Apps.Logger.Info("Ends contact_EvntCloseContactUs() in MainWindow.xaml.cs");
        }
        #endregion
    }
}
