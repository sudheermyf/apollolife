﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloLife.AdminUserControls
{
    /// <summary>
    /// Interaction logic for SetAutoSyncTime.xaml
    /// </summary>
    public partial class SetAutoSyncTime : UserControl
    {
        Timer currentTimer;
        public SetAutoSyncTime()
        {
            InitializeComponent();
            currentTimer = new Timer();
            currentTimer.Interval = 1000;
            currentTimer.Elapsed += delayTimer_Elapsed;
            currentTimer.Start();
        }
        public event EventHandler EvntCloseSetAutoSync;
        public event EventHandler autoSyncRefresh;
        public event EventHandler EvntSuccessAlert;
        public event EventHandler EvntWarningAlert;
        public event EventHandler EvntErrorAlert;
        Configuration config;

        void delayTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                tbCurrentTime.Text = DateTime.Now.ToShortTimeString();
            }));
        }
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            currentTimer.Stop();
            currentTimer = null;
            EvntCloseSetAutoSync(this, null);
        }

        private void btnSave_TouchDown(object sender, TouchEventArgs e)
        {
            if (CboHours.SelectedIndex > 0 && CboMinutes.SelectedIndex > 0)
            {
                string time = CboHours.Text + ":" + CboMinutes.Text;
                Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                config.AppSettings.Settings["AutoSyncTiming"].Value = time;
                config.AppSettings.Settings["AutoSyncEnabled"].Value = Convert.ToString(chkAutoSync.IsChecked);
                config.Save();
                autoSyncRefresh(this, null);
                string Message = "Auto Sync Details Saved Successfully !!";
                EvntSuccessAlert(Message, null);
                currentTimer.Stop();
                currentTimer = null;
                EvntCloseSetAutoSync(this, null);
            }
            else
            {
                EvntWarningAlert("Please select valid time.", null);
                //MessageBox.Show("Please select valid time.", "TOC", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DateTime savedDate = DateTime.Now;
            bool autoSyncEnabled = false;
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            if (!string.IsNullOrEmpty(config.AppSettings.Settings["AutoSyncTiming"].Value))
            {
                string autoSyncTime = config.AppSettings.Settings["AutoSyncTiming"].Value;
                string[] splitedAutoSyncTime = autoSyncTime.Split(':');
                if (splitedAutoSyncTime.Length > 0)
                {
                    CboHours.Text = Convert.ToString(splitedAutoSyncTime[0]);
                    CboMinutes.Text = Convert.ToString(splitedAutoSyncTime[1]);
                }
                chkAutoSync.IsChecked = Boolean.TryParse(config.AppSettings.Settings["AutoSyncEnabled"].Value, out autoSyncEnabled) ? autoSyncEnabled : false;
            }
        }
    }
}
