﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using System.Windows.Media.Animation;

namespace ApolloLife.Controls
{
    /// <summary>
    /// Interaction logic for CategoryUC.xaml
    /// </summary>
    public partial class CategoryUC : UserControl
    {
        public CategoryUC()
        {
            InitializeComponent();
        }
        #region Variables
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        public event EventHandler EvntCloseCategoryUC;
        public event EventHandler EvntOpenSubCategItem;
        public event EventHandler EvntHomeLogo;
        public event EventHandler EvntContactUs;
        public static int itemsCount;
        #endregion
        public Category categoryItem { get; set; }
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Storyboard CloseSideBarGrid_SB = TryFindResource("CloseSideBarGrid_SB") as Storyboard;
            CloseSideBarGrid_SB.Begin();
            Storyboard ClosePage_SB = TryFindResource("ClosePage_SB") as Storyboard;
            ClosePage_SB.Completed += new EventHandler(ClosePage_SB_Completed);
            ClosePage_SB.Begin();
        }

        void ClosePage_SB_Completed(object sender, EventArgs e)
        {
            EvntCloseCategoryUC(this, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (categoryItem != null)
            {
                this.DataContext = categoryItem;
                tbCategoryName.Text = categoryItem.Name.ToUpper();
                ICCategory.ItemsSource = categoryItem.LstSubCategories.Where(c => c.IsActive);
                stkBottomSlide.Visibility = categoryItem.LstSubCategories.Where(c => c.IsActive).ToList().Count > 8 ? Visibility.Visible : Visibility.Collapsed;
                itemsCount = categoryItem.LstSubCategories.Where(c => c.IsActive).ToList().Count;
            }
        }

        private void btnSubCategItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {

                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
            }
        }

        private void svSubCategory_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
            //tbTop.Text = svSubCategory.VerticalOffset.ToString();
            //tbBottom.Text = svSubCategory.ScrollableHeight.ToString();

            if (svSubCategory.VerticalOffset > 100)
            {
                stkTopSlide.Visibility = Visibility.Visible;
                stkBottomSlide.Visibility = Visibility.Visible;
            }
            if (svSubCategory.VerticalOffset == svSubCategory.ScrollableHeight)
            {
                stkBottomSlide.Visibility = Visibility.Collapsed;
                stkTopSlide.Visibility = Visibility.Visible;
            }
            if (svSubCategory.VerticalOffset == 0)
            {
                stkTopSlide.Visibility = Visibility.Collapsed;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svSubCategory_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svSubCategory_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svSubCategory_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    EvntOpenSubCategItem(btnTouchedItem, null);
                }
            }
        }

        private void svSubCategory_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnContactUs_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntContactUs(this, null);
        }

        private void btnLogo_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntHomeLogo(this, null);
        }
    }
}
