﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using System.Windows.Media.Animation;

namespace ApolloLife.Controls
{
    /// <summary>
    /// Interaction logic for ImageView.xaml
    /// </summary>
    public partial class ImageView : UserControl
    {
        public ImageView()
        {
            InitializeComponent();
        }
        public DocumentDetails documentDets { get; set; }
        public event EventHandler EvntCloseImageView;

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            Storyboard CloseImage_SB = TryFindResource("CloseImage_SB") as Storyboard;
            CloseImage_SB.Completed += new EventHandler(CloseImage_SB_Completed);
            CloseImage_SB.Begin();
        }

        void CloseImage_SB_Completed(object sender, EventArgs e)
        {
            EvntCloseImageView(this, null);            
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = documentDets;
        }
    }
}
