﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using Microsoft.Maps.MapControl.WPF;


namespace ApolloLife.Controls
{
    /// <summary>
    /// Interaction logic for ContactUC.xaml
    /// </summary>
    public partial class ContactUC : UserControl
    {
        public ContactUC()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseContactUs;

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            List<ContactUS> lstContacts = Apps.myDbContext.CONTACTUS.ToList();
            ICContactUS.ItemsSource = lstContacts;
            CreatePushPins(lstContacts);
        }
        private void CreatePushPins(List<ContactUS> lstContacts)
        {
            if (lstContacts != null)
            {
                lstContacts.ToList().ForEach(c =>
                {
                    Pushpin pin = CreatePushPin(c);
                    if (pin != null)
                    {
                        map.Children.Add(pin);
                    }
                });
            }
        }

        private Pushpin CreatePushPin(ContactUS locationData)
        {
            if (locationData != null && locationData.Location != null)
            {
                Pushpin pin = new Pushpin();
                pin.Location = new Location(Convert.ToDouble(locationData.Location.Latitude), Convert.ToDouble(locationData.Location.Longitude));
                return pin;
            }
            return null;
        }

        private void btnBack_TouchDown(object sender, TouchEventArgs e)
        {
            Storyboard ClosePage_SB = TryFindResource("ClosePage_SB") as Storyboard;
            ClosePage_SB.Completed += new EventHandler(ClosePage_SB_Completed);
            ClosePage_SB.Begin();
        }

        void ClosePage_SB_Completed(object sender, EventArgs e)
        {
            EvntCloseContactUs(this, null);
        }

        private void svContactLocation_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
