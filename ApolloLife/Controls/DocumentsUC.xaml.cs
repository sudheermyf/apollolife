﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloLife.Controls
{
    /// <summary>
    /// Interaction logic for DocumentsUC.xaml
    /// </summary>
    public partial class DocumentsUC : UserControl
    {
        public DocumentsUC()
        {
            InitializeComponent();
        }
        #region Variables
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        public event EventHandler EvntOpenDocumentItem;
        public event EventHandler EvntCloseDocumentUC;
        public event EventHandler EvntHomeLogo;
        public event EventHandler EvntContactUs;
        #endregion
        private void btnBack_TouchDown(object sender, TouchEventArgs e)
        {
            Storyboard CloseSideBarGrid_SB = TryFindResource("CloseSideBarGrid_SB") as Storyboard;
            CloseSideBarGrid_SB.Begin();
            Storyboard ClosePage_SB = TryFindResource("ClosePage_SB") as Storyboard;
            ClosePage_SB.Completed += new EventHandler(ClosePage_SB_Completed);
            ClosePage_SB.Begin();
        }

        void ClosePage_SB_Completed(object sender, EventArgs e)
        {
            EvntCloseDocumentUC(this, null);            
        }
        private void svDocument_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svDocument_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svDocument_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svDocument_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    EvntOpenDocumentItem(btnTouchedItem, null);
                }
            }
        }

        private void svDocument_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnDocumentItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {

                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
            }
        }

        private void btnLogo_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntHomeLogo(this, null);
        }

        private void btnContactUs_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntContactUs(this, null);
        }
    }
}
