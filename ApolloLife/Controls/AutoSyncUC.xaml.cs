﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using ApolloSync.InitiateCalls;
using ApolloSync.Logic;
using ApolloSync.Logic.Constants;
using System.IO;

namespace ApolloLife.Controls
{
    /// <summary>
    /// Interaction logic for AutoSyncUC.xaml
    /// </summary>
    public partial class AutoSyncUC : UserControl
    {
        public AutoSyncUC()
        {
            InitializeComponent();
        }
        private BackgroundWorker worker;
        public event EventHandler EvntCloseSetAutoSync;
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Apps.Logger.Info("Initiated UserControl_Loaded() in AutoSyncUC.xaml.cs");
            worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.RunWorkerAsync();
            Apps.Logger.Info("Ends UserControl_Loaded() in AutoSyncUC.xaml.cs");
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Apps.Logger.Info("Initiated worker_RunWorkerCompleted() in AutoSyncUC.xaml.cs");
            Apps.myDbContext = new ApolloDB.DBFolder.ApolloDBContext();
            Storyboard ClosePage_SB = TryFindResource("ClosePage_SB") as Storyboard;
            ClosePage_SB.Completed += ClosePage_SB_Completed;
            ClosePage_SB.Begin();
            Apps.Logger.Info("Ends worker_RunWorkerCompleted() in AutoSyncUC.xaml.cs");
        }

        void ClosePage_SB_Completed(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated ClosePage_SB_Completed() in AutoSyncUC.xaml.cs");
            EvntCloseSetAutoSync(this, null);
            Apps.Logger.Info("Ends ClosePage_SB_Completed() in AutoSyncUC.xaml.cs");
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Apps.Logger.Info("Initiated worker_ProgressChanged() in AutoSyncUC.xaml.cs");
            ProgressInformation progress = e.UserState as ProgressInformation;

            if (progress != null)
            {
                pbSplashScreen.Minimum = progress.StartIndex;
                pbSplashScreen.Maximum = progress.LastIndex;
                pbText.Text = progress.Message;
                pbSplashScreen.Value = e.ProgressPercentage;
            }
            Apps.Logger.Info("Ends worker_ProgressChanged() in AutoSyncUC.xaml.cs");
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Apps.Logger.Info("Initiated worker_DoWork() in AutoSyncUC.xaml.cs");
            AutoSync autoSync = new AutoSync();
            autoSync.syncProgressEvent += autoSync_syncProgressEvent;
            autoSync.StartDBSync();

            worker.ReportProgress(90, new ProgressInformation("Deleting Home SlideShow from Local Directory.", string.Empty, 1, 90, 100));
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + ApolloSyncImageFolderStructure.HomeSlideShow;
                if (Directory.Exists(path) && Directory.GetFiles(path).Count() > 0)
                {
                    List<string> homeSlideShowImgIds = Apps.myDbContext.HOMESLIDESHOW.Select(c => c.IMAGEID).ToList();
                    List<string> lstFileNamePaths = Directory.GetFiles(path).ToList();
                    List<string> lstFileNames = new List<string>();
                    lstFileNamePaths.ForEach(c =>
                        {
                            lstFileNames.Add(System.IO.Path.GetFileNameWithoutExtension(c));
                        });

                    List<string> lstDeleteFiles = lstFileNames.Where(c => !homeSlideShowImgIds.Contains(c)).ToList();
                    lstDeleteFiles.ForEach(c =>
                        {
                            string deletefile = lstFileNamePaths.FirstOrDefault(d => d.Contains(c));
                            if (!string.IsNullOrEmpty(deletefile))
                            {
                                try
                                {
                                    File.Delete(deletefile);
                                }
                                catch (Exception)
                                {

                                    //Log Here... If deleting file accessed by some other resource. Exception will throw.
                                }
                            }
                        });
                }
                Apps.Logger.Info("Ends worker_DoWork() in AutoSyncUC.xaml.cs");
            }
            catch (Exception ex)
            {
                Apps.Logger.Info("Ends worker_DoWork() -> Catch exception -> " + ex.Message + " in AutoSyncUC.xaml.cs");
                //Log Here
            }
        }

        void autoSync_syncProgressEvent(object sender, EventArgs e)
        {
            Apps.Logger.Info("Initiated autoSync_syncProgressEvent() in AutoSyncUC.xaml.cs");
            if (sender is ProgressInformation)
            {
                ProgressInformation progress = (ProgressInformation)sender;
                worker.ReportProgress(progress.CurrentIndex, progress);
            }
            Apps.Logger.Info("Ends autoSync_syncProgressEvent() in AutoSyncUC.xaml.cs");
        }
    }
}
