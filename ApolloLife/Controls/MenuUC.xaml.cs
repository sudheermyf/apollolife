﻿using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ApolloLife.Controls
{
    /// <summary>
    /// Interaction logic for MenuUC.xaml
    /// </summary>
    public partial class MenuUC : UserControl
    {
        #region Variables

        private DispatcherTimer timerImageChange;
        private Image[] ImageControls;
        private List<ImageSource> Images = new List<ImageSource>();
        private static string[] ValidImageExtensions = new[] { ".png" };//, ".jpg", ".jpeg", ".bmp", ".gif" };
        private static string[] TransitionEffects = new[] { "Fade" };
        private string TransitionType, strImagePath = "";
        private int CurrentSourceIndex, CurrentCtrlIndex, EffectIndex = 0, IntervalTimer = 1;

        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        public event EventHandler EvntCloseMenuUC;
        public event EventHandler EvntOpenCategoryItem;
        public event EventHandler EvntContactUs;
        #endregion
        public MenuUC()
        {
            InitializeComponent();
            //Initialize Image control, Image directory path and Image timer.
            IntervalTimer = Convert.ToInt32(ConfigurationManager.AppSettings["HomeSlideIntervalTime"]);
            strImagePath = ConfigurationManager.AppSettings["MenuImagePath"];
            ImageControls = new[] { myImage, myImage2 };

            LoadImageFolder(strImagePath);

            timerImageChange = new DispatcherTimer();
            timerImageChange.Interval = new TimeSpan(0, 0, IntervalTimer);
            timerImageChange.Tick += new EventHandler(timerImageChange_Tick);
        }
        #region MenuSlideShowMethod

        private void LoadImageFolder(string folder)
        {
            ErrorText.Visibility = Visibility.Collapsed;
            var sw = System.Diagnostics.Stopwatch.StartNew();
            if (!System.IO.Path.IsPathRooted(folder))
                folder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, folder); //System.IO.Path.Combine(Environment.CurrentDirectory, folder);
            if (!System.IO.Directory.Exists(folder))
            {
                ErrorText.Text = "Please Upload Menu Slide Images to display "; // + Environment.NewLine + folder;
                ErrorText.Visibility = Visibility.Visible;
                return;
            }
            Random r = new Random();
            var sources = from file in new System.IO.DirectoryInfo(folder).GetFiles().AsParallel()
                          where ValidImageExtensions.Contains(file.Extension, StringComparer.InvariantCultureIgnoreCase)
                          orderby r.Next()
                          select CreateImageSource(file.FullName, true);
            Images.Clear();
            Images.AddRange(sources);
            sw.Stop();
            Console.WriteLine("Total time to load {0} images: {1}ms", Images.Count, sw.ElapsedMilliseconds);
        }

        private ImageSource CreateImageSource(string file, bool forcePreLoad)
        {
            if (forcePreLoad)
            {
                var src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(file, UriKind.Absolute);
                src.CacheOption = BitmapCacheOption.OnLoad;
                src.EndInit();
                src.Freeze();
                return src;
            }
            else
            {
                var src = new BitmapImage(new Uri(file, UriKind.Absolute));
                src.Freeze();
                return src;
            }
        }

        private void timerImageChange_Tick(object sender, EventArgs e)
        {
            PlaySlideShow();
        }

        private void PlaySlideShow()
        {
            try
            {
                if (Images.Count == 0)
                {
                    return;
                }
                var oldCtrlIndex = CurrentCtrlIndex;
                CurrentCtrlIndex = (CurrentCtrlIndex + 1) % 2;
                CurrentSourceIndex = (CurrentSourceIndex + 1) % Images.Count;

                Image imgFadeOut = ImageControls[oldCtrlIndex];
                Image imgFadeIn = ImageControls[CurrentCtrlIndex];
                ImageSource newSource = Images[CurrentSourceIndex];
                imgFadeIn.Source = newSource;

                TransitionType = TransitionEffects[EffectIndex].ToString();

                Storyboard StboardFadeOut = (Resources[string.Format("{0}Out", TransitionType.ToString())] as Storyboard).Clone();
                StboardFadeOut.Begin(imgFadeOut);
                Storyboard StboardFadeIn = Resources[string.Format("{0}In", TransitionType.ToString())] as Storyboard;
                StboardFadeIn.Begin(imgFadeIn);
            }
            catch (Exception ex)
            {
            }
        }
        #endregion


        private void svCategoryList_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnCategoryItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {

                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
            }
        }

        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            timerImageChange.Stop();
            timerImageChange = null;
            EvntCloseMenuUC(this, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            MenuContext.ItemsSource = Apps.myDbContext.CATEGORY.ToList().Where(c => c.IsActive).OrderBy(c => c.Order);
            PlaySlideShow();
            timerImageChange.IsEnabled = true;
        }

        private void svCategoryList_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svCategoryList_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCategoryList_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svCategoryList_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    if (btnTouchedItem != null)
                    {
                        Category category = btnTouchedItem.DataContext as Category;
                        if (category != null)
                        {
                            EvntOpenCategoryItem(category, null);
                        }
                    }
                }
            }
        }

        private void btnContactUs_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntContactUs(this, null);
        }
    }
}
