﻿using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloLife.Controls
{
    /// <summary>
    /// Interaction logic for VideoView.xaml
    /// </summary>
    public partial class VideoView : UserControl
    {
        public VideoView()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseVideoView;
        public DocumentDetails documentDets { get; set; }
        public string mediaPath { get; set; }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (documentDets != null)
            {
                this.DataContext = documentDets;
                mediaPath = documentDets.FileLocation;
                if (!string.IsNullOrEmpty(mediaPath))
                {
                    MEVideo.Source = new System.Uri(mediaPath);
                    MEVideo.Play();
                    MEVideo.Position = new TimeSpan(0, 0, 2);
                }
                else
                    EvntCloseVideoView(this, null);
            }
        }
        private void MEVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            //close video on media end
            EvntCloseVideoView(this, null);
        }

        private void btnCloseButton_TouchDown(object sender, TouchEventArgs e)
        {
            EvntCloseVideoView(this, null);
        }

        private void btnPlay_TouchDown(object sender, TouchEventArgs e)
        {
            MEVideo.Play();
            btnPause.Visibility = Visibility.Visible;
            btnPlay.Visibility = Visibility.Collapsed;
        }

        private void btnPause_TouchDown(object sender, TouchEventArgs e)
        {
            MEVideo.Pause();
            btnPause.Visibility = Visibility.Collapsed;
            btnPlay.Visibility = Visibility.Visible;
        }

        private void btnStop_TouchDown(object sender, TouchEventArgs e)
        {
            MEVideo.Stop();
            MEVideo.Position = new TimeSpan(0, 0, 2);
            btnPlay.Visibility = Visibility.Visible;
            btnPause.Visibility = Visibility.Collapsed;
        }
    }
}
