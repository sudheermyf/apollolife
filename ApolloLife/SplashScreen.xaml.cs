﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Threading;
using System.Windows.Shapes;

namespace ApolloLife
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        public SplashScreen()
        {
            InitializeComponent();
        }
        BackgroundWorker bgw = null;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            bgw = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true };
            bgw.WorkerReportsProgress = true;
            bgw.WorkerSupportsCancellation = true;
            bgw.DoWork += bgw_DoWork;
            bgw.ProgressChanged += bgw_ProgressChanged;
            bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
            bgw.RunWorkerAsync();
        }
       
        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblLoadingText.Text = e.UserState as string;
        }
        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                bgw = null;
                this.Hide();
                MainWindow HomePage = new MainWindow();
                HomePage.ShowDialog();
                this.Close();
            }
            catch (Exception ex)
            {
                Apps.Logger.ErrorException("Exception " + ex.Message, ex);
            }
        }
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            bgw.ReportProgress(1, "Loading Database...");
            Apps.myDbContext = new ApolloDB.DBFolder.ApolloDBContext();
            Apps.myDbContext.FEEDBACKQUESTIONS.ToList();
            bgw.ReportProgress(2, "Loading Database...... Please wait.........");

            bgw.ReportProgress(3, "Intiating Feedback Questions...");
        }
    }
}
