﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApolloLife
{
    public class Utilities
    {
        public static string SlideShowServerUrl
        {
            get { return ConfigurationManager.AppSettings["SlideShowserverUrl"]; }
        }

        public static string SlideShowHostedService
        {
            get { return ConfigurationManager.AppSettings["SlideShowHostedService"]; }
        }

        public static string UserName
        {
            get { return ConfigurationManager.AppSettings["UserName"]; }
        }

        public static string Password
        {
            get { return ConfigurationManager.AppSettings["Password"]; }
        }
    }
}
